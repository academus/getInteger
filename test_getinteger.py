import unittest
from simulado2 import solution

class MyTest(unittest.TestCase):
    def test(self):
        check_integer = solution
        self.assertEqual(check_integer(6, 20), 3)

if __name__ == '__main__':
    unittest.main()

