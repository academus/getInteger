#Get Integer

That function return if a range of A..B have two consectives integer which the product result in the same value from the range.

Example:

A=6 and B=20, the function should return 3.
3*2 = 6
3*4 = 12
5*4 = 20
