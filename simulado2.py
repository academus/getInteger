# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A, B):
    if A > B:
        return 0

    init = int(B%A)
    a=1
    result = 0
    count = 0
    for y in range(A):
        #print(y)
        if (y * (y+1)) == A:
            result = y
            #print(result) 
    var = result +1
    if result > 0:

        for x in range(A, B):
            while result <= B:
                #print(result)
                if x == (var * (result + 1)):
                    count += 1

                result += 1
            result = var-1
        if count > 1:
            #print(count)
            return var 

    return 0

#print(solution(6, 20))
#print(solution(21, 29))
#print(solution(30, 56))
